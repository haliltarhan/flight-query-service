# Flight Management API Project

- This project contains a Flight Management REST API Service Implementation for a Java Tech Assessment. 
- The project is implemented with Java JDK 11, Spring Boot, Postgresql, H2 Database (testing), Lombok, JUnit 5, Swagger, Flyway, Slf4j along with Maven Build Tool.

## Author

- [Halil Hakan Tarhan](mailto::haliltarhan at gmail.com) - haliltarhan [at] gmail.com

## Tech Stack and Architectural Decisions

* Java 11 JDK is prefered for java version. (OpenJDK 64-Bit Server VM AdoptOpenJDK (build 11.0.9.1+1-202011062342))

* Spring Boot (2.7.3) is used for main Java Framework, it's almost de facto industry standart for Java ecosystem. That provides built-in (or ready to plug-in) modules for many requirements such as; project initialization, data management, rest api controllers, database migration, unit and integration tests, packaging, api documentation etc.

* H2 Database is used for portability and tests. H2 provides a fast in-memory database that supports JDBC API and R2DBC access, with a small (2mb) footprint. Supports embedded and server modes as well as a browser based console application.

* Postgresql or H2 DB is ready to use with the solution. It's already configured for the Postgresql notation as the initial configuration.

* Lombok as Java annotation library is used to write less and clean code for POJOs, constructors and builders, helps to reduce boilerplate code. If your IDE does not have it already installed, please visit https://projectlombok.org/setup/ .

* Swagger/Spring Fox is used for API Documentation.

* Flyway is a version control library for Database, from the initialization step to the latest version of the schema. Sample data is loaded at the beginning of the service's startup process.

* Simple Logging Facade for Java (Slf4j) used for logging purpose.

* JUnit 5 is used for unit tests and MockMVC package is used for Integration tests for controller layer.

* Maven is used for build tool/dependency management solution.

* **CQRS** pattern is used for the service implementation to separate the Read/Write concerns in more elegant way.

## How to access the API Documentation

- **Swagger API** Documentation can be accessed from the URL below. It will be accessible when the service is started and ready to serve requests.

- In addition to providing documentation for the services, this page can be used for functional testing.

- Each endpoint can be reviewed with it's request/response structure, object details and description about their functionality.

```
  http://localhost:8080/swagger-ui/index.html
```

## Application settings for default profile

 - The application.yml file is responsible for default profile settings, that can be found under /flight-service/src/main/resources/ folder.

 - Application runs on **8080** port, can be changed from configuration.

 - Database is configured for **Postgresql instance**, expects the configuration below:

```
   datasource:
    url: jdbc:postgresql://${RDS_HOSTNAME:localhost}:${RDS_PORT:5432}/${RDS_DB_NAME:postgres}
    username: ${RDS_USERNAME:postgres}
    password: ${RDS_PASSWORD:postgres}
    driverClassName: org.postgresql.Driver
```

 - Flyway reads sql files for initial schema creation and test data preparation, the files can be found in the /flight-service/src/main/resources/db/migration/postgres folder.

- API Documentation's context path is also can be updated manipulating the **swagger-ui.path** attribute.

```
spring:
  application:
    name: flight-service
  flyway:
    enabled: 'true'
    baseline-on-migrate: 'true'
    schemas: flight_schema
  jpa:
    properties:
      hibernate:
        dialect: org.hibernate.dialect.PostgreSQL95Dialect
    database: postgresql
    database-platform: org.hibernate.dialect.PostgreSQL95Dialect
    show-sql: 'true'
    hibernate:
      ddl-auto: none
  datasource:
    url: jdbc:postgresql://${RDS_HOSTNAME:localhost}:${RDS_PORT:5432}/${RDS_DB_NAME:postgres}
    username: ${RDS_USERNAME:postgres}
    password: ${RDS_PASSWORD:postgres}
    driverClassName: org.postgresql.Driver
  h2:
    console:
      enabled: 'true'
  database:
    driverClassName: org.postgresql.Driver
  sql.init.platform: postgres

caching:
  spring:
    flightCacheTTL: 120000

server:
  port: 8080
  ssl:
    enabled: false

springdoc:
  swagger-ui.path: /swagger-ui.html

spring.flyway.locations: db/migration/postgres
```

## How to Run Application

- To be able to run this service application your environment must have JDK 11 and Maven Build Tool and 8080 port of your environment must not be in use by another application.

## Build

- To compile with Maven, go to the root folder of the project and execute command below

```
  mvn compile
```

## Run
- To run with Maven, execute command below in the root folder of the project.

```
  mvn exec:java -Dexec.mainClass=com.assessment.flightservice.FlightServiceApplication
```
## Running Unit Tests

- To run the unit tests that covers service layer functionality, and also integration tests that covers REST API Controllers tests.

```
  mvn clean test  
```

## Build and Run as Docker Image
- To build the project as docker image use command below.

```
  docker build -t flight-service:0.1 .
```
- To run the project use command below, This will map 8080 port of your environment to the docker container's 8080 port. (directed to host's postgresql instance)

```
  docker container run -p 8080:8080 -e RDS_HOSTNAME=host.docker.internal flight-service:0.1
```

## Postman Collection for Test

- Postman collection is also shared within the source code. It's located under the docs folder of the project as Tech Assesment.postman_collection file.

## H2 Database Administration Panel Access

* To access H2 Database Administration Panel, simply visit the URL below. 

```
  http://localhost:8080/h2-console/
```

* Use options below to connect the H2 Console UI.
- Saved Settings:	Generic H2 (Embedded)
- Setting Name:	Generic H2 (Embedded)
- Driver Class:	org.h2.Driver
- JDBC URL:	jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;MODE=postgresql;INIT=CREATE SCHEMA IF NOT EXISTS flight_schema;DATABASE_TO_UPPER=false
- User Name:	sa
- Password:	 (empty)
