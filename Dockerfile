FROM maven:3.6.3 AS maven

WORKDIR /
COPY . /
# Compile and package the application to an executable JAR
RUN mvn package

# Use Java 11 JDK
FROM adoptopenjdk/openjdk11:alpine-jre

ARG JAR_FILE=flight-service-1.0.0.RELEASE.jar

WORKDIR /opt/app

COPY --from=maven /target/${JAR_FILE} /opt/app/
EXPOSE 8080

ENTRYPOINT ["java","-jar","flight-service-1.0.0.RELEASE.jar"]