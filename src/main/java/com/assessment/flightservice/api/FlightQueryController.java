package com.assessment.flightservice.api;

import com.assessment.flightservice.application.component.Constants;
import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import com.assessment.flightservice.application.dto.base.ApiResponse;
import com.assessment.flightservice.application.dto.base.ApiResponseCode;
import com.assessment.flightservice.application.service.FlightQueryServiceFactory;
import com.assessment.flightservice.application.service.impl.FlightQueryServiceImpl;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Tag(name = FlightQueryController.TAG)
@ApiResponses(
    value = {
      @io.swagger.annotations.ApiResponse(
          code = Constants.SUCCESS_CODE,
          message = Constants.SUCCESS_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.SYSTEM_ERROR_CODE,
          message = Constants.SYSTEM_ERROR_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.INVALID_REQUEST_CODE,
          message = Constants.INVALID_REQUEST_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.RESOURCE_NOT_FOUND_CODE,
          message = Constants.RESOURCE_NOT_FOUND_MESSAGE)
    })
@RequestMapping(value = {"/flight"})
@RequiredArgsConstructor
public class FlightQueryController {

  static final String TAG = "Flight Query Controller";

  private final FlightQueryServiceFactory flightQueryServiceFactory;
  private final FlightQueryServiceImpl flightQueryService;

  /**
   * Method to list all Flights.
   *
   * @return ApiResponse<List<FlightDto>>
   */
  @Operation(
      summary = "Flight List",
      description = "Flight List Endpoint, lists all defined flights with single request",
      tags = FlightQueryController.TAG)
  @GetMapping(value = "/list")
  public ApiResponse<List<FlightDto>> getAllFlights() {
    log.info("FlightQueryController /list endpoint called");
    return ApiResponse.success(flightQueryService.getAll(), ApiResponseCode.SUCCESS.getMessage());
  }

  /**
   * Method to get a flight by Id.
   *
   * @return ApiResponse<FlightDto>
   */
  @Operation(
      summary = "Flight Get Endpoint by Flight Id",
      description = "Get Flight by Id Endpoint",
      tags = FlightQueryController.TAG)
  @GetMapping(value = "/{flightId}")
  public ApiResponse<FlightDto> getFlight(@PathVariable(name = "flightId") Integer flightId) {
    log.info("FlightQueryController /flight/{} endpoint called", flightId);
    return ApiResponse.success(
        flightQueryService.getById(flightId), ApiResponseCode.SUCCESS.getMessage());
  }

  /**
   * Method for Flight search with SearchRequest and Pagination objects.
   *
   * @return ApiResponse<Page<FlightDtoExtended>>
   */
  @Operation(
      summary = "Flight search with SearchRequest and Pagination",
      description =
          "This endpoint is used for the search operation using SearchRequest and Pagination objects. \n\r SearchRequest object's all attributes are optional except originCode and destinationCode. \n\r Pagination is supported, page parameter is an index for the result's page, size defines the each page's size. Sort field can be used with FlightDto's attribute name with Asc or Desc directions like duration,Asc or price,Desc or can be ignored totally.",
      tags = FlightQueryController.TAG)
  @PostMapping(value = "/search")
  public ApiResponse<Page<FlightDtoExtended>> searchFlights(
      @RequestBody(required = false) @Valid SearchRequest request, Pageable pageable) {
    log.info(
        "FlightQueryController /search endpoint called with request:{}, pageable:{}",
        request,
        pageable);

    Page<FlightDtoExtended> s = flightQueryServiceFactory.search(request, pageable);
    return ApiResponse.success(s, ApiResponseCode.SUCCESS.getMessage());
  }
}
