package com.assessment.flightservice.api;

import com.assessment.flightservice.application.component.Constants;
import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.base.ApiResponse;
import com.assessment.flightservice.application.dto.base.ApiResponseCode;
import com.assessment.flightservice.application.service.FlightCommandService;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Tag(name = FlightCommandController.TAG)
@ApiResponses(
    value = {
      @io.swagger.annotations.ApiResponse(
          code = Constants.SUCCESS_CODE,
          message = Constants.SUCCESS_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.SYSTEM_ERROR_CODE,
          message = Constants.SYSTEM_ERROR_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.INVALID_REQUEST_CODE,
          message = Constants.INVALID_REQUEST_MESSAGE),
      @io.swagger.annotations.ApiResponse(
          code = Constants.RESOURCE_NOT_FOUND_CODE,
          message = Constants.RESOURCE_NOT_FOUND_MESSAGE)
    })
@RequestMapping(value = {"/flight"})
@RequiredArgsConstructor
public class FlightCommandController {

  static final String TAG = "Flight Command Controller";

  private final FlightCommandService flightCommandService;

  /**
   * Method to create a Flight.
   *
   * @return FlightDto
   */
  @Operation(
      summary = "Flight Create",
      description =
          "Flight Create Endpoint\n\r Accepts a FlightDto and executes the create operation.",
      tags = FlightCommandController.TAG)
  @PostMapping
  public ApiResponse<FlightDto> saveFlight(@RequestBody @Valid FlightDto flightDto) {
    return ApiResponse.success(
        flightCommandService.save(flightDto), ApiResponseCode.SUCCESS.getMessage());
  }

  /**
   * Method to update a Flight.
   *
   * @return FlightDto
   */
  @Operation(
      summary = "Flight Update",
      description = "Flight Update Endpoint. Updates a flight entity that already created.",
      tags = FlightCommandController.TAG)
  @PutMapping(value = "/{flightId}")
  public ApiResponse<FlightDto> updateFlight(
      @PathVariable(value = "flightId", required = true) @Valid Integer flightId,
      @Valid @RequestBody(required = true) FlightDto flightDto) {
    return ApiResponse.success(
        flightCommandService.update(flightId, flightDto), ApiResponseCode.SUCCESS.getMessage());
  }

  /**
   * Method to delete a Flight.
   *
   * @return FlightDto
   */
  @Operation(
      summary = "Flight Delete",
      description = "Flight Delete Endpoint. Deletes a flight entity that already created.",
      tags = FlightCommandController.TAG)
  @DeleteMapping(value = "/{flightId}")
  public ApiResponse<Void> deleteFlight(
      @PathVariable(value = "flightId", required = true) @Valid Integer flightId) {
    flightCommandService.delete(flightId);
    return ApiResponse.noContentSuccess();
  }
}
