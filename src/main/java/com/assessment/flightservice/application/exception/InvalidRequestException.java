package com.assessment.flightservice.application.exception;

import com.assessment.flightservice.application.dto.base.ApiResponseCode;

public class InvalidRequestException extends FlightServiceException {

  /** */
  private static final long serialVersionUID = -6401234647417619354L;

  public InvalidRequestException(String message) {
    super(ApiResponseCode.INVALID_REQUEST, message);
  }

  public InvalidRequestException(ApiResponseCode errorCode, String message) {
    super(errorCode, message);
  }
}
