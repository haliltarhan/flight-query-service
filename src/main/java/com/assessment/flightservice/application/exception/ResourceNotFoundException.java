package com.assessment.flightservice.application.exception;

import com.assessment.flightservice.application.dto.base.ApiResponseCode;

public class ResourceNotFoundException extends FlightServiceException {

  /** */
  private static final long serialVersionUID = -7460123454776892003L;

  public ResourceNotFoundException(Integer recordId) {
    super(
        ApiResponseCode.RESOURCE_NOT_FOUND, String.format("Record not found with ID:%s", recordId));
  }

  public ResourceNotFoundException(String message) {
    super(ApiResponseCode.RESOURCE_NOT_FOUND, message);
  }

  public ResourceNotFoundException(ApiResponseCode errorCode, String message) {
    super(errorCode, message);
  }
}
