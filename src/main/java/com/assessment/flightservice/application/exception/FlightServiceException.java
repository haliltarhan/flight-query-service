package com.assessment.flightservice.application.exception;

import com.assessment.flightservice.application.dto.base.ApiResponseCode;
import lombok.Getter;

public class FlightServiceException extends RuntimeException {

  /** */
  private static final long serialVersionUID = 2967121274425691076L;

  @Getter private ApiResponseCode errorCode;

  public FlightServiceException(ApiResponseCode errorCode) {
    super(errorCode.getMessage());
    this.errorCode = errorCode;
  }

  public FlightServiceException(ApiResponseCode errorCode, String message) {
    super(message);
    this.errorCode = errorCode;
  }
}
