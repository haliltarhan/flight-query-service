package com.assessment.flightservice.application.model.base;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
@MappedSuperclass
public abstract class IdentifiableEntity<T extends Number> extends BaseEntity {

  private static final long serialVersionUID = 1399123431234031020L;

  @Getter
  @Setter
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", updatable = false, nullable = false)
  protected T id;

  @Override
  public int hashCode() {
    return id.hashCode();
  }

  @Override
  public boolean equals(Object obj) {

    if (this == obj) {
      return true;
    } else if (obj == null) {
      return false;
    } else if (!(obj instanceof IdentifiableEntity)) {
      return false;
    }

    IdentifiableEntity<?> other = (IdentifiableEntity<?>) obj;
    return Objects.equals(this.id, other.getId());
  }
}
