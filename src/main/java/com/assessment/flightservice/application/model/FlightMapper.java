package com.assessment.flightservice.application.model;

import com.assessment.flightservice.application.dto.FlightDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class FlightMapper extends ModelToEntityMapper<FlightDto, FlightEntity> {

  public FlightDto from(FlightEntity entity) {
    return FlightDto.builder()
        .id(entity.getId())
        .flightNumber(entity.getFlightNumber())
        .originCode(entity.getOriginCode())
        .destinationCode(entity.getDestinationCode())
        .departureTime(entity.getDepartureTime())
        .arrivalTime(entity.getArrivalTime())
        .price(entity.getPrice())
        .currency(entity.getCurrency())
        .duration(entity.getDuration())
        .build();
  }

  public FlightEntity to(FlightDto model) {
    FlightEntity flightEntity = new FlightEntity();
    flightEntity.setFlightNumber(model.getFlightNumber());
    flightEntity.setOriginCode(model.getOriginCode());
    flightEntity.setDestinationCode(model.getDestinationCode());
    flightEntity.setDepartureTime(model.getDepartureTime());
    flightEntity.setArrivalTime(model.getArrivalTime());
    flightEntity.setPrice(model.getPrice());
    flightEntity.setCurrency(model.getCurrency());
    return flightEntity;
  }
}
