package com.assessment.flightservice.application.model.base;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

  private static final long serialVersionUID = 7021234584544333718L;
}
