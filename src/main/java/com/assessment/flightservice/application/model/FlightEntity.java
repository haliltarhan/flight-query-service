package com.assessment.flightservice.application.model;

import com.assessment.flightservice.application.model.base.IdentifiableEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.hibernate.annotations.Formula;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor(force = true)
@AllArgsConstructor
@Table(name = "flight", schema = "flight_schema")
@JsonInclude(value = Include.NON_NULL)
public class FlightEntity extends IdentifiableEntity<Integer> {

  private static final long serialVersionUID = -2051234955931430176L;

  private static final String FLIGHT_NUMBER = "flight_number";
  private static final String ORIGIN_CODE = "origin_code";
  private static final String DESTINATION_CODE = "destination_code";
  private static final String DEPARTURE_TIME = "departure_time";
  private static final String ARRIVAL_TIME = "arrival_time";
  private static final String PRICE = "price";
  private static final String CURRENCY = "currency";

  @NonNull
  @Column(name = FLIGHT_NUMBER)
  String flightNumber;

  @NonNull
  @Column(name = ORIGIN_CODE)
  String originCode;

  @NonNull
  @Column(name = DESTINATION_CODE)
  String destinationCode;

  @NonNull
  @Column(name = DEPARTURE_TIME)
  Time departureTime;

  @NonNull
  @Column(name = ARRIVAL_TIME)
  Time arrivalTime;

  @NonNull
  @Column(name = PRICE)
  Float price;

  @NonNull
  @Column(name = CURRENCY)
  String currency;

  @Formula(
      "CASE WHEN flight_schema.DATE_PART('hour', arrival_time::time - departure_time::time) * 60 "
          + " + flight_schema.DATE_PART('minute', arrival_time::time - departure_time::time) > 0 THEN "
          + "   flight_schema.DATE_PART('hour', arrival_time::time - departure_time::time) * 60 +  flight_schema.DATE_PART('minute', arrival_time::time - departure_time::time) "
          + " ELSE 1440+ flight_schema.DATE_PART('hour', arrival_time::time - departure_time::time) * 60 + flight_schema.DATE_PART('minute', arrival_time::time - departure_time::time) END")
  Long duration;

  // @Formula("CASE WHEN datediff('MINUTE',departure_time,arrival_time) > 0 THEN
  // datediff('MINUTE',departure_time,arrival_time) ELSE
  // 1440+(datediff('MINUTE',departure_time,arrival_time)) END")
  // Long duration;
}
