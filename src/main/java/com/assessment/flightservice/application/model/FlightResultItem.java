package com.assessment.flightservice.application.model;

import java.sql.Time;

public interface FlightResultItem {

  Integer getId();

  Integer getSolutionId();

  Integer getElement();

  Integer getOrdinal();

  Float getTotalPrice();

  Integer getTotalDuration();

  Integer getSolutionStopCount();

  String getFlightNumber();

  String getOriginCode();

  String getDestinationCode();

  Time getDepartureTime();

  Time getArrivalTime();

  Float getPrice();

  String getCurrency();

  Long getDuration();
}
