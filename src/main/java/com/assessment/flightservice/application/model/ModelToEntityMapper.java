package com.assessment.flightservice.application.model;

public abstract class ModelToEntityMapper<M, T> {

  public abstract M from(T table);

  public abstract T to(M model);
}
