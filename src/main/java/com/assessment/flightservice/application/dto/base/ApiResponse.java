package com.assessment.flightservice.application.dto.base;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(value = Include.NON_NULL)
public class ApiResponse<T> {

  private ApiResponseCode code;

  private String message;

  private T data;

  public static <T> ApiResponse<T> success(T data, String message) {
    return ApiResponse.<T>builder()
        .code(ApiResponseCode.SUCCESS)
        .data(data)
        .message(message)
        .build();
  }

  public static <T> ApiResponse<T> success() {
    return ApiResponse.success(null, null);
  }

  public static <T> ApiResponse<T> noContentSuccess() {
    return ApiResponse.success(null, ApiResponseCode.SUCCESS.getMessage());
  }

  @JsonGetter("code")
  public int getCode() {
    return code.getCode();
  }
}
