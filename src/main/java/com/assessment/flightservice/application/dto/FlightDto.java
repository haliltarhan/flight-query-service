package com.assessment.flightservice.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.sql.Time;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Class representing a flight data model in the service.")
public class FlightDto implements Serializable {
  /** */
  private static final long serialVersionUID = -3536042915955629364L;

  @ApiModelProperty(
      notes = "Unique identifier of the flight. No two flights can have the same id.",
      example = "1",
      required = false,
      position = 0)
  Integer id;

  @ApiModelProperty(
      notes = "Flight number of the flight.",
      example = "A101",
      required = true,
      position = 1)
  @NotNull(message = "Flight number must not be null!")
  @Size(min = 4, max = 4, message = "Flight number must have 4 characters.")
  String flightNumber;

  @ApiModelProperty(
      notes = "Origin code of the flight, airport codes.",
      example = "AMS for Amsterdam, DEL, IST etc.",
      required = true,
      position = 2)
  @NotNull(message = "Origin code must not be null!")
  @Size(min = 3, max = 3, message = "Origin code must have 3 characters")
  String originCode;

  @ApiModelProperty(
      notes = "Destination code of the flight, airport codes.",
      example = "AMS for Amsterdam, DEL, IST etc.",
      required = true,
      position = 2)
  @NotNull(message = "Destination code must not be null!")
  @Size(min = 3, max = 3, message = "Destination code must have 3 characters")
  String destinationCode;

  @NotNull(message = "Departure time must not be null!")
  Time departureTime;

  @NotNull(message = "Arrival time must not be null!")
  Time arrivalTime;

  @NotNull(message = "Price must not be null!")
  Float price;

  @NotNull(message = "Currency must not be null!")
  @Size(min = 3, max = 6, message = "Currency must have between 3-6 characters")
  String currency;

  Long duration;
}
