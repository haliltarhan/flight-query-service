package com.assessment.flightservice.application.dto;

import com.assessment.flightservice.application.dto.filter.SearchFilter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"originCode", "destinationCode", "filter", "multiStop"})
@ApiModel(
    description =
        "This object is used by the search operation.\n\r All fields are optional except originCode and destinationCode, filter object and its attributes can be used separetely.\n\r Pagination is supported, page parameter is an index for the result's page, size defines the each page's size. Sort field can be used with FlightDto's attribute name with Asc or Desc directions like name,Asc or preparationTime,Desc or can be ignored totally.")
public class SearchRequest {

  @ApiModelProperty(
      notes = "Origin code of the flight, airport codes.",
      example = "AMS for Amsterdam, DEL, IST etc.",
      required = true,
      position = 2)
  @NotNull(message = "Origin code must not be null!")
  @Size(min = 3, max = 3, message = "Origin code must have 3 characters")
  private String originCode;

  @ApiModelProperty(
      notes = "Destination code of the flight, airport codes.",
      example = "AMS for Amsterdam, DEL, IST etc.",
      required = true,
      position = 2)
  @NotNull(message = "Destination code must not be null!")
  @Size(min = 3, max = 3, message = "Destination code must have 3 characters")
  private String destinationCode;

  @ApiModelProperty(
      notes = "Filter object to add conditions in addition to origin destination code",
      required = false,
      position = 3)
  private SearchFilter filter;

  @ApiModelProperty(
      notes = "Parameter to enable or disable the multi stop query functionality",
      required = false,
      position = 4)
  @Builder.Default
  private Boolean multiStop = false;
}
