package com.assessment.flightservice.application.dto.base;

import lombok.Getter;

@Getter
public enum ApiResponseCode {
  SUCCESS(200, "Request successfully completed."),
  SYSTEM_ERROR(500, "System error is occurred!"),
  INVALID_REQUEST(400, "Invalid request!"),
  RESOURCE_NOT_FOUND(404, "Resource not found!");

  private int code;

  private String message;

  ApiResponseCode(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return this.code;
  }

  public String getMessage() {
    return this.message;
  }
}
