package com.assessment.flightservice.application.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(
    description =
        "Class representing a result set item that wraps flight(s) with extended attributes (metadata) in the service.")
public class FlightDtoExtended implements Serializable {

  @ApiModelProperty(notes = "Id of the result set item.", example = "1, 2, 3 etc.")
  Integer id;

  @ApiModelProperty(
      notes = "The stop count exists in the flight proposed for the queried origin destination.",
      example = "1, 2, 3 etc.")
  Integer stops;

  @ApiModelProperty(
      notes = "Total price of the flight(s) item.",
      example = "80, 300, 500 etc. in given currency")
  Float totalPrice;

  @ApiModelProperty(
      notes = "Total duration in MINUTES of the flight(s) item.",
      example = "60,120 or 480 etc.")
  Integer totalDuration;

  @ApiModelProperty(
      notes = "Flight(s) provided for the queried origin destination.",
      example = "60,120 or 480 etc.")
  @Builder.Default
  List<FlightDto> flights = new ArrayList<>();
}
