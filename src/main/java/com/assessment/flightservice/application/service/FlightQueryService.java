package com.assessment.flightservice.application.service;

import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FlightQueryService {

  FlightDto getById(Integer id);

  List<FlightDto> getAll();

  Page<FlightDtoExtended> search(SearchRequest searchDto, Pageable pageable);
}
