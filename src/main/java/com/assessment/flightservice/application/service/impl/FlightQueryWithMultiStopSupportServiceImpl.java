package com.assessment.flightservice.application.service.impl;

import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import com.assessment.flightservice.application.model.FlightEntity;
import com.assessment.flightservice.application.model.FlightMapper;
import com.assessment.flightservice.application.model.FlightResultItem;
import com.assessment.flightservice.application.repository.FlightRepository;
import com.assessment.flightservice.application.service.FlightQueryService;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service("queryServiceWithMultiStop")
@RequiredArgsConstructor
public class FlightQueryWithMultiStopSupportServiceImpl implements FlightQueryService {

  private final FlightRepository repository;
  private final FlightMapper flightMapper;

  // List all flights without any filter or pagination support
  @Override
  public List<FlightDto> getAll() {
    List<FlightEntity> flights = repository.findAll();
    if (flights != null && !flights.isEmpty()) {
      return flights.stream().map(r -> flightMapper.from(r)).collect(Collectors.toList());
    }
    return new ArrayList<FlightDto>();
  }

  // Search flight with multiple criteria and cache support
  @Override
  @Cacheable(
      cacheNames = {"flightCache"},
      key = "new org.springframework.cache.interceptor.SimpleKey(#searchRequest, #pageable)",
      unless = "!(#result.hasContent())")
  public Page<FlightDtoExtended> search(SearchRequest searchRequest, Pageable pageable) {

    Pageable unsortedPageable =
        PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(), Sort.unsorted());

    Page<FlightResultItem> flightQueryResult =
        repository.doFlightQuery(
            searchRequest.getOriginCode(),
            searchRequest.getDestinationCode(),
            (searchRequest.getFilter() != null
                    && searchRequest.getFilter().getFlightNumber() != null
                ? searchRequest.getFilter().getFlightNumber().getValue()
                : null),
            (searchRequest.getFilter() != null && searchRequest.getFilter().getDuration() != null
                ? searchRequest.getFilter().getDuration().getLower()
                : null),
            (searchRequest.getFilter() != null && searchRequest.getFilter().getDuration() != null
                ? searchRequest.getFilter().getDuration().getUpper()
                : null),
            (searchRequest.getFilter() != null && searchRequest.getFilter().getPrice() != null
                ? searchRequest.getFilter().getPrice().getLower()
                : null),
            (searchRequest.getFilter() != null && searchRequest.getFilter().getPrice() != null
                ? searchRequest.getFilter().getPrice().getUpper()
                : null),
            unsortedPageable);

    if (flightQueryResult != null && !flightQueryResult.isEmpty()) {

      List<FlightDtoExtended> content = new ArrayList<>();
      AtomicBoolean nextFlightIsStop = new AtomicBoolean(false);

      flightQueryResult.stream()
          .forEach(
              fqr -> {
                FlightDto flightDto =
                    FlightDto.builder()
                        .id(fqr.getId())
                        .flightNumber(fqr.getFlightNumber())
                        .originCode(fqr.getOriginCode())
                        .destinationCode(fqr.getDestinationCode())
                        .departureTime(fqr.getDepartureTime())
                        .arrivalTime(fqr.getArrivalTime())
                        .duration(fqr.getDuration())
                        .price(fqr.getPrice())
                        .currency(fqr.getCurrency())
                        .build();

                if (nextFlightIsStop.get()) {
                  content.get(content.size() - 1).getFlights().add(flightDto);
                } else {
                  FlightDtoExtended flightDtoExtended =
                      FlightDtoExtended.builder()
                          .id(fqr.getSolutionId())
                          .stops(fqr.getSolutionStopCount())
                          .totalDuration(fqr.getTotalDuration())
                          .totalPrice(fqr.getTotalPrice())
                          .build();
                  flightDtoExtended.getFlights().add(flightDto);
                  content.add(flightDtoExtended);
                }

                if (fqr.getOrdinal() < fqr.getSolutionStopCount()) {
                  nextFlightIsStop.getAndSet(true);
                } else {
                  nextFlightIsStop.getAndSet(false);
                }
              });
      return new PageImpl<FlightDtoExtended>(content, pageable, content.size());
    }

    return new PageImpl<FlightDtoExtended>(new ArrayList<FlightDtoExtended>(), pageable, 0L);
  }

  @Override
  public FlightDto getById(Integer id) {
    return null;
  }
}
