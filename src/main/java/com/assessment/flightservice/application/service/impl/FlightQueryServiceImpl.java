package com.assessment.flightservice.application.service.impl;

import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import com.assessment.flightservice.application.exception.ResourceNotFoundException;
import com.assessment.flightservice.application.model.FlightEntity;
import com.assessment.flightservice.application.model.FlightMapper;
import com.assessment.flightservice.application.repository.FlightRepository;
import com.assessment.flightservice.application.repository.FlightSpecificationsBuilder;
import com.assessment.flightservice.application.service.FlightQueryService;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service("queryServiceWithoutMultiStop")
@Slf4j
@RequiredArgsConstructor
public class FlightQueryServiceImpl implements FlightQueryService {

  private final FlightRepository repository;
  private final FlightMapper flightMapper;

  // List all flights without any filter or pagination support
  @Override
  public List<FlightDto> getAll() {
    List<FlightEntity> flights = repository.findAll();
    if (flights != null && !flights.isEmpty()) {
      return flights.stream().map(r -> flightMapper.from(r)).collect(Collectors.toList());
    }
    return new ArrayList<FlightDto>();
  }

  // Search flight with multiple criteria and cache support
  @Override
  @Cacheable(
      cacheNames = {"flightCache"},
      key = "new org.springframework.cache.interceptor.SimpleKey(#searchRequest, #pageable)",
      unless = "!(#result.hasContent())")
  public Page<FlightDtoExtended> search(SearchRequest searchRequest, Pageable pageable) {

    Specification<FlightEntity> specification =
        FlightSpecificationsBuilder.originCodeEquals(searchRequest.getOriginCode());
    specification =
        specification.and(
            FlightSpecificationsBuilder.destinationCodeEquals(searchRequest.getDestinationCode()));

    // Process filter if exists
    if (searchRequest.getFilter() != null) {
      if (searchRequest.getFilter().getFlightNumber() != null
          && searchRequest.getFilter().getFlightNumber().getValue() != null) {
        specification =
            specification.and(
                FlightSpecificationsBuilder.flightNumberEquals(
                    searchRequest.getFilter().getFlightNumber().getValue()));
      }
      if (searchRequest.getFilter().getPrice() != null
          && searchRequest.getFilter().getPrice().getLower() != null
          && searchRequest.getFilter().getPrice().getUpper() != null) {
        specification =
            specification.and(
                FlightSpecificationsBuilder.priceBetweenRange(
                    searchRequest.getFilter().getPrice().getLower(),
                    searchRequest.getFilter().getPrice().getUpper()));
      }
      if (searchRequest.getFilter().getDuration() != null
          && searchRequest.getFilter().getDuration().getLower() != null
          && searchRequest.getFilter().getDuration().getUpper() != null) {
        specification =
            specification.and(
                FlightSpecificationsBuilder.durationBetweenRange(
                    searchRequest.getFilter().getDuration().getLower(),
                    searchRequest.getFilter().getDuration().getUpper()));
      }
    }

    Page<FlightEntity> flights = repository.findAll(specification, pageable);
    if (flights != null && !flights.isEmpty()) {
      List<FlightDtoExtended> content =
          flights.getContent().stream()
              .map(flight -> flightMapper.from(flight))
              .map(
                  fligthDto -> {
                    FlightDtoExtended flightDtoExtended =
                        FlightDtoExtended.builder()
                            .id(fligthDto.getId())
                            .stops(1)
                            .totalDuration(fligthDto.getDuration().intValue())
                            .totalPrice(fligthDto.getPrice())
                            .build();

                    flightDtoExtended.getFlights().add(fligthDto);

                    return flightDtoExtended;
                  })
              .collect(Collectors.toList());

      return new PageImpl<FlightDtoExtended>(content, pageable, flights.getTotalElements());
    }

    return new PageImpl<FlightDtoExtended>(new ArrayList<FlightDtoExtended>(), pageable, 0L);
  }

  // Evict flightCache entries for each given period
  @CacheEvict(value = "flightCache", allEntries = true)
  @Scheduled(fixedRateString = "${caching.spring.flightCacheTTL}")
  public void emptyFlightCache() {
    log.info("Clearing the flight cache");
  }

  // get FlighEntity by using primary key (id)
  @Override
  public FlightDto getById(Integer id) {
    Optional<FlightEntity> flightEntity = repository.findById(id);
    if (!flightEntity.isPresent()) {
      log.warn("FlightEntity with id:{} is not found", id);
      throw new ResourceNotFoundException(id);
    }
    log.info("FlightEntity with id:{} is accessed", id);
    return flightMapper.from(flightEntity.get());
  }
}
