package com.assessment.flightservice.application.service;

import com.assessment.flightservice.application.dto.FlightDto;
import javax.validation.Valid;

public interface FlightCommandService {

  FlightDto save(@Valid FlightDto flightDto);

  void delete(Integer id);

  FlightDto update(Integer id, @Valid FlightDto flightDto);
}
