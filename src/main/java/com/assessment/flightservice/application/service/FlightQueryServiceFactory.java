package com.assessment.flightservice.application.service;

import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FlightQueryServiceFactory {

  private final FlightQueryService flightQueryServiceWithoutMultiStop;

  private final FlightQueryService flightQueryServiceWithMultiStop;

  public FlightQueryServiceFactory(
      @Qualifier("queryServiceWithoutMultiStop")
          FlightQueryService flightQueryServiceWithoutMultiStop,
      @Qualifier("queryServiceWithMultiStop") FlightQueryService flightQueryServiceWithMultiStop) {
    this.flightQueryServiceWithoutMultiStop = flightQueryServiceWithoutMultiStop;
    this.flightQueryServiceWithMultiStop = flightQueryServiceWithMultiStop;
  }

  public Page<FlightDtoExtended> search(@Valid SearchRequest searchRequest, Pageable pageable) {
    if (searchRequest.getMultiStop()) {
      return flightQueryServiceWithMultiStop.search(searchRequest, pageable);
    } else {
      return flightQueryServiceWithoutMultiStop.search(searchRequest, pageable);
    }
  }
}
