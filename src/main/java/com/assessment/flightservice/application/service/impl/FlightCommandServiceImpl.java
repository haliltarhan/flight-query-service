package com.assessment.flightservice.application.service.impl;

import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.exception.ResourceNotFoundException;
import com.assessment.flightservice.application.model.FlightEntity;
import com.assessment.flightservice.application.model.FlightMapper;
import com.assessment.flightservice.application.repository.FlightRepository;
import com.assessment.flightservice.application.service.FlightCommandService;
import java.util.Optional;
import javax.transaction.Transactional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Validated
@Service
@Slf4j
@RequiredArgsConstructor
public class FlightCommandServiceImpl implements FlightCommandService {

  private final FlightRepository repository;
  private final FlightMapper flightMapper;

  @Transactional
  @Override
  public FlightDto save(@Valid FlightDto flightDto) {
    FlightEntity flightEntity = repository.save(flightMapper.to(flightDto));
    log.info("FlightEntity saved with id:{}", flightEntity.getId());
    return flightMapper.from(flightEntity);
  }

  @Transactional
  @Override
  public void delete(Integer id) {

    Optional<FlightEntity> flightEntity = repository.findById(id);
    if (!flightEntity.isPresent()) {
      log.warn("FlightEntity with id:{} is not found", id);
      throw new ResourceNotFoundException(id);
    }
    repository.deleteById(id);
    log.info("FlightEntity with id:{} is deleted", id);
  }

  @Transactional
  @Override
  public FlightDto update(Integer id, @Valid FlightDto flightDto) {

    Optional<FlightEntity> flightEntity = repository.findById(id);
    if (!flightEntity.isPresent()) {
      log.warn("FlightEntity with id:{} is not found", id);
      throw new ResourceNotFoundException(id);
    }

    FlightEntity updatedFlight = flightMapper.to(flightDto);
    updatedFlight.setId(id);
    updatedFlight = repository.save(updatedFlight);
    log.info("FlightEntity with id:{} is updated", updatedFlight.getId());
    return flightMapper.from(updatedFlight);
  }
}
