package com.assessment.flightservice.application.repository;

import com.assessment.flightservice.application.model.FlightEntity;
import org.springframework.data.jpa.domain.Specification;

/**
 * This FlightSpecificationsBuilder class is used for populating queries for Flight search
 * operation, using attributes from SearchRequest class.
 */
public class FlightSpecificationsBuilder {

  private static final String FLIGHT_NUMBER = "flightNumber";
  private static final String ORIGIN_CODE = "originCode";
  private static final String DESTINATION_CODE = "destinationCode";
  private static final String PRICE = "price";
  private static final String DURATION = "duration";

  /** Add query criteria for flight query, adding field originCode with EQUAL expression. */
  public static Specification<FlightEntity> originCodeEquals(String originCode) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.<String>get(ORIGIN_CODE), originCode);
  }

  /** Add query criteria for flight query, adding field destinationCode with EQUAL expression. */
  public static Specification<FlightEntity> destinationCodeEquals(String destinationCode) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.<String>get(DESTINATION_CODE), destinationCode);
  }

  /** Add query criteria for flight price range with BETWEEN expression. */
  public static Specification<FlightEntity> priceBetweenRange(Float lower, Float upper) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.between(root.<Float>get(PRICE), lower, upper);
  }

  /** Add query criteria for flight query, adding field flightNumber with EQUAL expression. */
  public static Specification<FlightEntity> flightNumberEquals(String flightNumber) {
    return (root, query, criteriaBuilder) ->
        criteriaBuilder.equal(root.<String>get(FLIGHT_NUMBER), flightNumber);
  }

  /** Add query criteria for flight query, duration field with BETWEEN expression. */
  public static Specification<FlightEntity> durationBetweenRange(Float lower, Float upper) {

    return (root, query, criteriaBuilder) ->
        criteriaBuilder.between(root.get(DURATION), lower, upper);
  }
}
