package com.assessment.flightservice.application.repository;

import com.assessment.flightservice.application.model.FlightEntity;
import com.assessment.flightservice.application.model.FlightResultItem;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FlightRepository
    extends JpaRepository<FlightEntity, Integer>, JpaSpecificationExecutor<FlightEntity> {

  static final String FLIGHT_QUERY_WITH_MULTI_STOPS =
      " SELECT * from (  "
          + " 	SELECT final_solution.id as id,  "
          + "	 final_solution.solution_id as solutionId,  "
          + "	 final_solution.element as element,  "
          + "	 final_solution.ordinal as ordinal,  "
          + "	 SUM(final_solution.price) OVER (PARTITION BY final_solution.solution_id) as totalPrice,  "
          + "	 SUM(final_solution.duration) OVER (PARTITION BY final_solution.solution_id) as totalDuration,  "
          + "	 final_solution.solution_stop_count as solutionStopCount,  "
          + "	 final_solution.flight_number as flightNumber,  "
          + "	 final_solution.origin_code as originCode,  "
          + "	 final_solution.destination_code as destinationCode,  "
          + "	 final_solution.departure_time as departureTime,  "
          + "	 final_solution.arrival_time as arrivalTime,  "
          + "	 final_solution.price as price,  "
          + "	 final_solution.currency as currency,  "
          + "	 final_solution.duration as duration  "
          + "	   from (  "
          + "	     Select element, ordinal, row_number as solution_id, fsf.*,  "
          + "	  CASE WHEN DATE_PART('hour', fsf.arrival_time ::::time - fsf.departure_time ::::time) * 60   "
          + "	   + DATE_PART('minute', fsf.arrival_time ::::time - fsf.departure_time ::::time) > 0 THEN   "
          + "	     DATE_PART('hour', fsf.arrival_time ::::time - fsf.departure_time ::::time) * 60 + DATE_PART('minute', fsf.arrival_time ::::time - fsf.departure_time ::::time)   "
          + "	   ELSE 1440+DATE_PART('hour', fsf.arrival_time ::::time - fsf.departure_time ::::time) * 60 + DATE_PART('minute', fsf.arrival_time ::::time - fsf.departure_time ::::time) end as duration,  "
          + "	  array_length(solution.path,1) as solution_stop_count  "
          + "		 from (  "
          + "			 	with flights_and_paths as (  "
          + "			 			with path_table as (  "
          + "							with graph as (  "
          + "								with RECURSIVE flight_distance_graph (flight_number, origin_code, destination_code, path,path_origin) AS (  "
          + "								    SELECT f.flight_number, f.origin_code, f.destination_code, ARRAY[ f.id ::::text] as path, ARRAY[f.origin_code ::::text] as path_origin  "
          + "								    FROM flight_schema.flight as f   "
          + "								UNION ALL  "
          + "								    select g.flight_number, g.origin_code, f.destination_code, g.path || ARRAY[f.id ::::text], g.path_origin || ARRAY[f.origin_code ::::text]  "
          + "								    FROM flight_schema.flight as f  "
          + "								    JOIN flight_distance_graph as g   "
          + "								    ON f.origin_code = g.destination_code  "
          + "								    and f.id ::::text != ALL(g.path)  "
          + "								    and f.destination_code ::::text != ALL(g.path_origin)  "
          + "								)  "
          + "								select flight_number ,origin_code,destination_code, path, path_origin  "
          + "								from flight_distance_graph   "
          + "								where (origin_code = :origin_code and destination_code = :destination_code)   "
          + "               and (flight_number = :flight_number or :flight_number is null) "
          + "								and array_length(path,1) between 1 and 4  "
          + "								order by  array_length(path,1) asc  "
          + "								limit 20  "
          + "							)   "
          + "						select ROW_NUMBER () OVER (ORDER BY array_length(path,1)) , (graph.path) from graph  "
          + "						)   "
          + "					Select * from flight_schema.flight fl, path_table   "
          + "					where  fl.id = any(path_table.path ::::INT[])  "
          + "			  	)  "
          + "		  	  	select * from flights_and_paths   "
          + "		  	    join unnest(flights_and_paths.path) WITH ORDINALITY AS arr(element, ordinal)  "
          + "		  	      on flights_and_paths.id = arr.element ::::INT  "
          + "		  	    order by row_number,arr.ordinal  "
          + "		  ) solution, flight_schema.flight fsf  "
          + "	  where fsf.id = element ::::INT  "
          + "  order by row_number, ordinal) final_solution ) final_resultset "
          + "   where "
          + "   (totalDuration between :duration_low and :duration_high or :duration_low is null or :duration_high is null) "
          + "   and (totalPrice between :price_low and :price_high or :price_low is null or :price_high is null) ";

  Optional<FlightEntity> findById(Integer id);

  Optional<FlightEntity> findByFlightNumber(String flightNumber);

  @Query(
      countQuery = "Select count(*) from (" + FLIGHT_QUERY_WITH_MULTI_STOPS + ") count_query",
      value = FLIGHT_QUERY_WITH_MULTI_STOPS,
      nativeQuery = true)
  Page<FlightResultItem> doFlightQuery(
      @Param("origin_code") String originCode,
      @Param("destination_code") String destinationCode,
      @Param("flight_number") String flightNumber,
      @Param("duration_low") Float durationLow,
      @Param("duration_high") Float durationHigh,
      @Param("price_low") Float priceLow,
      @Param("price_high") Float priceHigh,
      Pageable pageable);
}
