package com.assessment.flightservice.application.component;

import com.assessment.flightservice.application.dto.base.ApiResponse;
import com.assessment.flightservice.application.dto.base.ApiResponseCode;
import com.assessment.flightservice.application.exception.FlightServiceException;
import com.assessment.flightservice.application.exception.InvalidRequestException;
import com.assessment.flightservice.application.exception.ResourceNotFoundException;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
@Slf4j
public class ApiResponseAdvice {

  @ExceptionHandler(InvalidRequestException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiResponse<?> handleInvalidRequestException(InvalidRequestException e) {

    log.error("Invalid request! {}", e.getMessage());

    return ApiResponse.builder().code(e.getErrorCode()).message(e.getMessage()).build();
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ApiResponse<?> handleResourceNotFoundException(ResourceNotFoundException e) {

    log.warn("Resource not found for the request! {}", e.getMessage());

    return ApiResponse.builder().code(e.getErrorCode()).message(e.getMessage()).build();
  }

  @ExceptionHandler({
    MissingServletRequestParameterException.class,
    MethodArgumentTypeMismatchException.class,
    HttpMessageNotReadableException.class
  })
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiResponse<?> handleSpringRequestValidationException(Exception e) {

    log.error("Invalid request! {}", e.getMessage());

    return ApiResponse.builder()
        .message("Invalid request!")
        .code(ApiResponseCode.INVALID_REQUEST)
        .build();
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiResponse<?> handleInvalidRequestException(NoHandlerFoundException e) {

    log.error("No handler is found! {}", e.getMessage());

    return ApiResponse.builder()
        .code(ApiResponseCode.INVALID_REQUEST)
        .message("Requested path (" + e.getRequestURL() + ") is not found!")
        .build();
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiResponse<?> handleDataIntegrityViolationException(DataIntegrityViolationException e) {

    log.error("DataIntegrityViolationException occured! {}", e.getMessage());

    return ApiResponse.builder()
        .message("Database Error or Data Integrity Exception!")
        .code(ApiResponseCode.INVALID_REQUEST)
        .build();
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ApiResponse<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {

    log.error("Invalid request! {}", e.getMessage());

    String message = ApiResponseCode.SYSTEM_ERROR.getMessage();

    if (e.getBindingResult().getFieldErrorCount() > 0) {
      message =
          e.getBindingResult().getFieldErrors().stream()
              .map(DefaultMessageSourceResolvable::getDefaultMessage)
              .collect(Collectors.joining("\n"));
    }

    return ApiResponse.builder().code(ApiResponseCode.INVALID_REQUEST).message(message).build();
  }

  @ExceptionHandler(FlightServiceException.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ApiResponse<?> handleApiException(FlightServiceException e) {

    if (e.getErrorCode() == ApiResponseCode.SYSTEM_ERROR) log.error("Api exception!", e);
    else log.info("Api exception! {}", e.getMessage());

    return ApiResponse.builder().code(e.getErrorCode()).message(e.getMessage()).build();
  }

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ApiResponse<?> handleException(Exception e) {

    log.error("Unexpected system error is occurred!", e);

    return ApiResponse.builder()
        .code(ApiResponseCode.SYSTEM_ERROR)
        .message(ApiResponseCode.SYSTEM_ERROR.getMessage())
        .build();
  }
}
