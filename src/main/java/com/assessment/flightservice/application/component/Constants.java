package com.assessment.flightservice.application.component;

public class Constants {

  public static final int SUCCESS_CODE = 200;
  public static final String SUCCESS_MESSAGE = "Request successfully completed.";
  public static final int SYSTEM_ERROR_CODE = 500;
  public static final String SYSTEM_ERROR_MESSAGE = "System error is occurred!";
  public static final int INVALID_REQUEST_CODE = 400;
  public static final String INVALID_REQUEST_MESSAGE = "Invalid request!";
  public static final int RESOURCE_NOT_FOUND_CODE = 404;
  public static final String RESOURCE_NOT_FOUND_MESSAGE = "Resource not found!";
}
