package com.assessment.flightservice.application.config;

import java.util.Collections;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .useDefaultResponseMessages(false)
        .apiInfo(apiInfo())
        .select()
        .apis(RequestHandlerSelectors.basePackage("com.assessment.flightservice.api"))
        .paths(PathSelectors.any())
        .build();
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        "Flight Management Service API",
        "APIs for Flight Management Service\n\rThis API is responsible for;\n\r CRUD operations and Search-API functionality for Flight, \n\rPersisting them in a DB,\n\r Execution of custom search request(s) on the saved data.",
        "1.0",
        "Terms of service",
        new Contact("Halil Hakan TARHAN", "***", "haliltarhan@gmail.com"),
        "License of API",
        "API license URL",
        Collections.emptyList());
  }
}
