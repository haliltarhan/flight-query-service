package com.assessment.flightservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling // enabling scheduling
@EnableCaching // enabling caching
@SpringBootApplication
@ComponentScan(basePackages = {"com.assessment"})
public class FlightServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(FlightServiceApplication.class, args);
  }
}
