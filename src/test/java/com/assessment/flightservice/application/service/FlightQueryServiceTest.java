package com.assessment.flightservice.application.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.assessment.flightservice.TestClassesOrder;
import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.FlightDtoExtended;
import com.assessment.flightservice.application.dto.SearchRequest;
import com.assessment.flightservice.application.dto.filter.RangeFilter;
import com.assessment.flightservice.application.dto.filter.SearchFilter;
import com.assessment.flightservice.application.model.FlightEntity;
import com.assessment.flightservice.application.repository.FlightRepository;
import com.assessment.flightservice.application.service.impl.FlightQueryServiceImpl;
import java.util.List;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(locations = "classpath:/test-config.xml")
@SpringBootTest
@TestClassesOrder(1)
public class FlightQueryServiceTest {

  @Autowired private FlightQueryServiceImpl subject;

  @Autowired private FlightRepository repository;

  @Test
  @Order(0)
  void list_all_flights_successfully() {

    List<FlightDto> allFlights = subject.getAll();

    List<FlightEntity> allFlightList = repository.findAll();

    // Assert
    assertTrue(allFlights != null);
    assertTrue(!allFlights.isEmpty() && allFlights.size() > 0);
    assertEquals(allFlightList.size(), allFlights.size());
  }

  @Test
  @Order(1)
  void search_ams_del_flights_with_page_limit_successfully() {

    SearchRequest searchRequest = new SearchRequest();
    searchRequest.setOriginCode("AMS");
    searchRequest.setDestinationCode("DEL");

    Pageable pageable = PageRequest.of(0, 3, Sort.unsorted());

    Page<FlightDtoExtended> flightPage = subject.search(searchRequest, pageable);

    // Assert
    assertTrue(flightPage != null);
    assertTrue(flightPage.getContent() != null);
    assertEquals(1, flightPage.getContent().size());
  }

  @Test
  @Order(2)
  void search_all_bom_del_flights_successfully() {

    SearchRequest searchRequest = new SearchRequest();
    searchRequest.setOriginCode("BOM");
    searchRequest.setDestinationCode("DEL");

    Pageable pageable = PageRequest.of(0, 100, Sort.unsorted());

    Page<FlightDtoExtended> flightPage = subject.search(searchRequest, pageable);

    // Assert
    assertTrue(flightPage != null);
    assertTrue(flightPage.getContent() != null);
    assertEquals(5, flightPage.getContent().size());
  }

  @Test
  @Order(3)
  void search_bom_del_flights_with_price_range_filter_successfully() {

    SearchRequest searchRequest = new SearchRequest();
    searchRequest.setOriginCode("BOM");
    searchRequest.setDestinationCode("DEL");
    SearchFilter filter = new SearchFilter();
    RangeFilter<Float> priceRange = new RangeFilter<Float>(70f, 90f);
    filter.setPrice(priceRange);
    searchRequest.setFilter(filter);

    Pageable pageable = PageRequest.of(0, 100, Sort.unsorted());

    Page<FlightDtoExtended> flightPage = subject.search(searchRequest, pageable);

    // Assert
    assertTrue(flightPage != null);
    assertTrue(flightPage.getContent() != null);
    assertEquals(3, flightPage.getContent().size());
  }
}
