package com.assessment.flightservice.application.service;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.assessment.flightservice.TestClassesOrder;
import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.service.util.FlightTestDataBuilder;
import javax.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig(locations = "classpath:/test-config.xml")
@SpringBootTest
@TestClassesOrder(2000)
public class FlightCommandServiceTest {

  @Autowired private FlightCommandService subject;

  @Test
  @Order(0)
  void save_flight_successfully() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setFlightNumber("Q888");
    // Act
    FlightDto result = subject.save(flightDto);

    // Assert
    assertTrue(result != null);
    assertTrue(result.getId() != null);
  }

  @Test
  @Order(1)
  void save_flight_throws_exception_when_flight_number_is_null() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setFlightNumber(null);

    // Act & Assert
    Assertions.assertThrows(ConstraintViolationException.class, () -> subject.save(flightDto));
  }

  @Test
  @Order(2)
  void save_flight_throws_exception_when_origin_code_is_null() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setOriginCode(null);

    // Act & Assert
    Assertions.assertThrows(ConstraintViolationException.class, () -> subject.save(flightDto));
  }

  @Test
  @Order(3)
  void save_flight_throws_exception_when_destination_code_is_null() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setDestinationCode(null);

    // Act & Assert
    Assertions.assertThrows(ConstraintViolationException.class, () -> subject.save(flightDto));
  }

  @Test
  @Order(4)
  void save_flight_throws_exception_when_departure_time_is_null() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setDepartureTime(null);

    // Act & Assert
    Assertions.assertThrows(ConstraintViolationException.class, () -> subject.save(flightDto));
  }

  @Test
  @Order(5)
  void save_flight_throws_exception_when_arrival_time_is_null() {
    // Arrange
    FlightDto flightDto = new FlightTestDataBuilder().build();
    flightDto.setArrivalTime(null);

    // Act & Assert
    Assertions.assertThrows(ConstraintViolationException.class, () -> subject.save(flightDto));
  }
}
