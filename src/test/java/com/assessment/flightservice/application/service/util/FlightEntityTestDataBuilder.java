package com.assessment.flightservice.application.service.util;

import com.assessment.flightservice.application.model.FlightEntity;
import java.sql.Time;

public class FlightEntityTestDataBuilder {

  public FlightEntity build() {

    FlightEntity flight = new FlightEntity();
    flight.setId(1);
    flight.setFlightNumber("F999");
    flight.setOriginCode("IST");
    flight.setDestinationCode("AMS");
    flight.setDepartureTime(Time.valueOf("13:00:00"));
    flight.setArrivalTime(Time.valueOf("17:00:00"));
    flight.setPrice(100f);
    flight.setCurrency("EURO");
    return flight;
  }
}
