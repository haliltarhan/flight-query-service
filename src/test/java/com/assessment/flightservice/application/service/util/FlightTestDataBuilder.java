package com.assessment.flightservice.application.service.util;

import com.assessment.flightservice.application.dto.FlightDto;
import java.sql.Time;

public class FlightTestDataBuilder {

  public FlightDto build() {

    return FlightDto.builder()
        .flightNumber("F999")
        .originCode("IST")
        .destinationCode("AMS")
        .departureTime(Time.valueOf("13:00:00"))
        .arrivalTime(Time.valueOf("17:00:00"))
        .price(100f)
        .currency("EURO")
        .build();
  }
}
