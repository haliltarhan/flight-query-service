package com.assessment.flightservice;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"com.assessment"})
public class SpringTestConfiguration {}
