package com.assessment.flightservice.api;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.assessment.flightservice.TestClassesOrder;
import com.assessment.flightservice.application.dto.FlightDto;
import com.assessment.flightservice.application.dto.base.ApiResponseCode;
import com.assessment.flightservice.application.service.util.FlightTestDataBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringJUnitConfig(locations = "classpath:/test-config.xml")
@SpringBootTest
@AutoConfigureMockMvc
@TestClassesOrder(1000)
public class FlightCommandControllerTest {

  @Autowired private MockMvc mockMvc;

  private static final String FLIGHT_NUMBER = "F999";
  private static final String ORIGIN_CODE = "IST";
  private static final String DESTINATION_CODE = "AMS";
  private static final String DEPARTURE_TIME = "13:00:00";
  private static final String ARRIVAL_TIME = "17:00:00";
  private static final Float PRICE = 100f;
  private static final String CURRENCY = "EURO";

  private static final Float PRICE_TOLERANCE = 0.0000001f;

  @Test
  @Order(0)
  void test_create_flight() throws Exception {

    FlightDto payload = new FlightTestDataBuilder().build();
    // Act
    mockMvc
        .perform(
            post("/flight").contentType(MediaType.APPLICATION_JSON).content(asJsonString(payload)))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is(ApiResponseCode.SUCCESS.getMessage())))
        .andExpect(jsonPath("$.data.flightNumber", is(FLIGHT_NUMBER)))
        .andExpect(jsonPath("$.data.originCode", is(ORIGIN_CODE)))
        .andExpect(jsonPath("$.data.destinationCode", is(DESTINATION_CODE)))
        .andExpect(jsonPath("$.data.departureTime", is(DEPARTURE_TIME)))
        .andExpect(jsonPath("$.data.arrivalTime", is(ARRIVAL_TIME)))
        .andExpect(jsonPath("$.data.price", closeTo(PRICE, PRICE_TOLERANCE)))
        .andExpect(jsonPath("$.data.currency", is(CURRENCY)));
  }

  @Test
  @Order(1)
  void test_flight_create_with_null_flight_number() throws Exception {

    FlightDto payload = new FlightTestDataBuilder().build();
    payload.setFlightNumber(null);

    // Act
    mockMvc
        .perform(
            post("/flight").contentType(MediaType.APPLICATION_JSON).content(asJsonString(payload)))
        // Assert
        .andExpect(status().isBadRequest())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", equalTo("Flight number must not be null!")))
        .andExpect(jsonPath("$.code", is(ApiResponseCode.INVALID_REQUEST.getCode())));
  }

  @Test
  @Order(2)
  void test_create_flight_and_update_it() throws Exception {

    FlightDto payload = new FlightTestDataBuilder().build();
    // Act
    MvcResult result =
        mockMvc
            .perform(
                post("/flight")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(payload)))
            // Assert
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.message", is(ApiResponseCode.SUCCESS.getMessage())))
            .andExpect(jsonPath("$.data.flightNumber", is(FLIGHT_NUMBER)))
            .andExpect(jsonPath("$.data.originCode", is(ORIGIN_CODE)))
            .andExpect(jsonPath("$.data.destinationCode", is(DESTINATION_CODE)))
            .andExpect(jsonPath("$.data.departureTime", is(DEPARTURE_TIME)))
            .andExpect(jsonPath("$.data.arrivalTime", is(ARRIVAL_TIME)))
            .andExpect(jsonPath("$.data.price", closeTo(PRICE, PRICE_TOLERANCE)))
            .andExpect(jsonPath("$.data.currency", is(CURRENCY)))
            .andReturn();

    String responseText = result.getResponse().getContentAsString();
    String id = JsonPath.parse(responseText).read("$.data.id").toString();

    // Update the name and instructions
    payload = payload.toBuilder().id(Integer.parseInt(id)).flightNumber("Q999").build();

    mockMvc
        .perform(
            put("/flight/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(payload)))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is(ApiResponseCode.SUCCESS.getMessage())))
        .andExpect(jsonPath("$.data.flightNumber", is("Q999")))
        .andExpect(jsonPath("$.data.originCode", is(ORIGIN_CODE)))
        .andExpect(jsonPath("$.data.destinationCode", is(DESTINATION_CODE)))
        .andExpect(jsonPath("$.data.departureTime", is(DEPARTURE_TIME)))
        .andExpect(jsonPath("$.data.arrivalTime", is(ARRIVAL_TIME)))
        .andExpect(jsonPath("$.data.price", closeTo(PRICE, PRICE_TOLERANCE)))
        .andExpect(jsonPath("$.data.currency", is(CURRENCY)));
  }

  @Test
  @Order(3)
  void test_create_and_delete_flight() throws Exception {

    FlightDto payload = new FlightTestDataBuilder().build();
    // Act
    MvcResult result =
        mockMvc
            .perform(
                post("/flight")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(asJsonString(payload)))
            // Assert
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.message", is(ApiResponseCode.SUCCESS.getMessage())))
            .andExpect(jsonPath("$.data.flightNumber", is(FLIGHT_NUMBER)))
            .andExpect(jsonPath("$.data.originCode", is(ORIGIN_CODE)))
            .andExpect(jsonPath("$.data.destinationCode", is(DESTINATION_CODE)))
            .andExpect(jsonPath("$.data.departureTime", is(DEPARTURE_TIME)))
            .andExpect(jsonPath("$.data.arrivalTime", is(ARRIVAL_TIME)))
            .andExpect(jsonPath("$.data.price", closeTo(PRICE, PRICE_TOLERANCE)))
            .andExpect(jsonPath("$.data.currency", is(CURRENCY)))
            .andReturn();

    String responseText = result.getResponse().getContentAsString();
    String id = JsonPath.parse(responseText).read("$.data.id").toString();

    mockMvc
        .perform(
            delete("/flight/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(payload)))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON));
  }

  static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
