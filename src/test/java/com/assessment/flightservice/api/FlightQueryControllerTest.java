package com.assessment.flightservice.api;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.assessment.flightservice.TestClassesOrder;
import com.assessment.flightservice.application.dto.SearchRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;

@SpringJUnitConfig(locations = "classpath:/test-config.xml")
@SpringBootTest
@AutoConfigureMockMvc
@TestClassesOrder(2)
public class FlightQueryControllerTest {

  @Autowired private MockMvc mockMvc;

  @Test
  @Order(0)
  void list_all_flights() throws Exception {

    // Act
    mockMvc
        .perform(get("/flight/list").contentType(MediaType.APPLICATION_JSON))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.data", notNullValue()));
    ;
  }

  @Test
  @Order(2)
  void search_ams_bom_flights_with_pagination_with_page_size_as_5() throws Exception {

    // Arrange
    // Empty search request
    SearchRequest searchRequest = new SearchRequest();
    searchRequest.setOriginCode("AMS");
    searchRequest.setDestinationCode("BOM");

    // Act
    mockMvc
        .perform(
            post("/flight/search?page=0&size=3")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(searchRequest)))
        // Assert
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.data.numberOfElements", is(3)));
  }

  static String asJsonString(final Object obj) {
    try {
      return new ObjectMapper().writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
