INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('A101','AMS','DEL','11:00','17:00',200,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('B101','AMS','BOM','12:00','19:30',750,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('C101','AMS','BLR','13:00','18:30',800,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('D101','AMS','MAA','09:00','15:00',600,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('E101','MAA','BOM','16:00','17:30',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('F101','BOM','DEL','20:30','21:30',80,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('G101','BOM','DEL','18:00','19:30',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('A201','LHR','DEL','11:30','17:00',600,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('B201','LHR','BOM','12:35','19:30',750,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('C201','LHR','BLR','13:45','18:30',800,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('D201','LHR','MAA','09:00','15:00',600,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('E201','DEL','BOM','18:45','20:15',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('F201','BOM','DEL','21:15','22:30',80,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('G201','BOM','DEL','20:20','22:30',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('H301','AMS','BOM','23:30','01:30',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('H302','BOM','DEL','22:30','01:00',80,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('H303','AMS','MAA','21:15','04:45',100,'EURO', now(), now());

INSERT INTO flight_schema.flight
(flight_number, origin_code, destination_code, departure_time, arrival_time, price, currency, created_at, modified_at)
VALUES('H401','AMS','BOM','12:30','11:30',100,'EURO', now(), now());
