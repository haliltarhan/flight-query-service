CREATE SCHEMA IF NOT EXISTS flight_schema;

CREATE TABLE if not exists flight_schema.flight (
	id serial NOT null constraint flight_pkey primary key,
	flight_number 		varchar(4) 	NOT NULL,			-- number of the flight
	origin_code	 		varchar(3) 	NOT NULL,		-- origin city code of the flight
	destination_code	varchar(3) 	NOT NULL,		-- destination city code of the flight
	departure_time		TIME 	NOT NULL,			-- departure_time of the flight
	arrival_time		TIME 	NOT NULL,			-- arrival_time of the flight
	price				NUMERIC(8,2) 	NOT NULL,	-- price of the flight
	currency			varchar(6) 		NOT NULL,	-- price of the flight
	created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	modified_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE flight_schema.flight  ADD CONSTRAINT FLIGHT_NUMBER_UNIQUE UNIQUE(flight_number);

CREATE INDEX  if not exists IDX_ORIGIN_DESTINATION_CODES ON flight_schema.flight(origin_code,destination_code);